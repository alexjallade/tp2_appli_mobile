package fr.uavignon.ceri.tp2;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;
import fr.uavignon.ceri.tp2.data.Book;

public class ListViewModel extends AndroidViewModel {

    private BookRepository Repository;
    private LiveData<List<Book>> allBooks;


    public ListViewModel (Application application) {
        super(application);
        Repository = new BookRepository(application);
        allBooks = Repository.getAllBooks();

    }
    public LiveData<List<Book>> getAllBooks() {
        return allBooks;
    }
}
