package fr.uavignon.ceri.tp2;
import android.app.Application;
import android.util.Log;

import fr.uavignon.ceri.tp2.data.Book;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class DetailViewModel extends AndroidViewModel {
    private BookRepository repository;
    private MutableLiveData<Book> searchBook;
    static long currentId = 11;

    public DetailViewModel(Application application) {
        super(application);
        repository = new BookRepository(application);
        searchBook = repository.getSearchBook();
    }

    public MutableLiveData<Book> getSearchBook() {
        return searchBook;
    }

    public void setSearchBook(long id) {
        repository.findBook(id);
        searchBook = repository.getSearchBook();
    }




    public void insertOrUpdateBook( Book book )
    {
        if ( (book.getId()!=-1)) {
            repository.updateBook(book);
        }
        else{
            currentId += 1;
            book.setId(currentId);
            repository.insertBook(book);

        }
    }

}
