package fr.uavignon.ceri.tp2;


import android.app.Application;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import fr.uavignon.ceri.tp2.data.Book;

import static fr.uavignon.ceri.tp2.BookRoomDatabase.databaseWriteExecutor;


public class BookRepository {

    private MutableLiveData<Book> searchResults = new MutableLiveData<Book>();
    private LiveData<List<Book>> allBooks;
    private boolean searchTitle;

    private BookDao bookDao;

    public BookRepository(Application application) {
        BookRoomDatabase db = BookRoomDatabase.getDatabase(application);
        bookDao = db.bookDao();
        allBooks = bookDao.getAllBooks();
    }

    public void insertBook(Book newbook) {
        databaseWriteExecutor.execute(() -> {
            bookDao.insertBook(newbook);
        });
    }

    public void deleteProduct(long id) {
        databaseWriteExecutor.execute(() -> {
            bookDao.deleteBook(id);
        });
    }





    public void updateBook(Book book) {
        databaseWriteExecutor.execute(() -> {
            bookDao.updateBook(book);
        });
    }


    public MutableLiveData<Book> getSearchBook() {
        return searchResults;
    }


    public void findBook(long id) {
        Future<Book> fproducts = databaseWriteExecutor.submit(() -> {
            return bookDao.getBook(id);
        });
        try {
            searchResults.setValue(fproducts.get());
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public LiveData<List<Book>> getAllBooks() {
        return allBooks;
    }




}
